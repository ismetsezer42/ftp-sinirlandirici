# Ftp Sınırlandırıcı

Örneğin site dosyalarınızı başkasıyla paylaşırken hepsini paylaşmak istemiyorsanız
bunu kullanabilirsiniz. Dizini sınırlandırarak kullanıcıların sadece belirli dosyalar üzerinde
işlem yapmasını sağlamaktadır.

### Testler
- Linux Ve Windows üzerinde test edilmiştir

Kodu kullanmak için ise kod içerisinde değişiklik yapıp pyinstaller ile derlenmiş halini oluşturduktan sonra
bağlantı yapmasını istediğiniz kişiye gönderip kullandırtabilirsiniz

Burada ki kod linux ortamı için pyinstaller ile derlenmiş halidir. Ftp host ve diğer bilgileri client.py içerisinde
değiştirip kullanabilirsiniz
